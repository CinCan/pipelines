set -e
set -x
set -u

# fix locale warnings, due to server image likely missing your ssh-supplied locale
for l in $(set | grep '^LC_' | sed 's/.*=//' | sort | uniq); do
    sudo locale-gen $l
done

sudo apt install --no-install-recommends postgresql
# if you need to rerun this script, you can either comment out the
# create commands, or to start with a fresh db, run "dropdb concdb"
# and "dropuser concdb" as the postgres user
sudo -u postgres createuser -P concdb
sudo -u postgres createdb -O concdb concdb


if [ -f secrets/basic_auth_pass -a -f secrets/db_pass -a -f dns_server_address ]; then
    echo found password and dns files
else
    echo "Create the files secrets/basic_auth_pass and secrets/db_pass, containing the passwords used for basic authentication and postgresql db account respectivel, and dns_server_address containing the dns server address, and hit enter to continue:"
    read asdf
fi

read basic_auth_pass < secrets/basic_auth_pass
read db_pass < secrets/db_pass
read dns_server_address < dns_server_address
test -f /usr/local/bin/concourse || {
  sudo wget -O /usr/local/bin/concourse.new \
       https://github.com/concourse/concourse/releases/download/v3.14.1/concourse_linux_amd64
  sudo chown root:root /usr/local/bin/concourse.new
  sudo chmod +x /usr/local/bin/concourse.new
  sudo mv /usr/local/bin/concourse.new /usr/local/bin/concourse
}
workdir=/var/lib/concourse/worker-work
sudo mkdir -p $workdir
sudo concourse quickstart \
	  --basic-auth-username=conker \
	  --basic-auth-password=$basic_auth_pass \
	  --worker-work-dir=$workdir --worker-garden-dns-server=$dns_server_address \
          --postgres-user=concdb --postgres-password=$db_pass --postgres-database=concdb

