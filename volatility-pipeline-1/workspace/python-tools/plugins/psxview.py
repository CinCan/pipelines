import util

def psxview(sample, profile):
    """ Execute Volatility's psxview plugin that finds hidden processes

    Keyword arguments:
    sample: path to memory sample
    profile: memory profile of the sample

    :return: dictionary containing Volatility JSON output from the executed plugins
    """

    params = ["-f", sample,
              "--profile=" + profile
             ]

    processes = util.execute_plugin(params, ["psxview"])
    hidden = []
    for row in processes["rows"]:
        if 'False' in row[4]:
            hidden.append([str(row[0]), str(row[2])])
    return processes, hidden
