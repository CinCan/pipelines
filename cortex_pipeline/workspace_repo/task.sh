#!/bin/sh
samples="resource-repo/samples/*"
output="./resource-dumped-processes/dump"

set -e

git clone resource-repo resource-dumped-processes

ls -la
cp /opt/Cortex-Analyzers/analyzers/Abuse_Finder/abusefinder.py resource-repo/python-tools/

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

for filename in $samples
do
    directory=$(basename ${filename%.*})
    if [ ! -d "$output/$directory"]; then
        mkdir $output/$directory
    fi
    python resource-repo/python-tools/cortex_abusefinder.py $filename $output/$directory
done

cd resource-dumped-processes
git add dump/*
git commit -m "dumped data"

