#!/bin/bash
# Script to create a list of pipelines

# Format to be used in pipeline readme's:
#	# <pipeline name>
#
#	<description>
#
#	### Tools run in the pipeline
#
#	<tools used>


# Cut off the old list of pipelines
sed -i '/### Description*/q' README.md

cat >> README.md << EOL
| Pipeline name  | Tools used  | Description  | Quick setup for the pilot environment |
|----------------|-------------|--------------|---------------------------------------|
EOL

# Dive into folders
find . -mindepth 1 -maxdepth 1 -type d 2>/dev/null |while read -r OUTPUT; do
	PIPELINE="$(echo "${OUTPUT}" | sed 's/.\///; s/\///')"
	PIPELINE_NAME=''
	SUMMARY=''
	TOOLS=''
	QUICK_SETUP=''

	# Add to list if a yml file exists
	YML_EXISTS=$(find "$PIPELINE"/. -maxdepth 1 -name "README.md" | wc -l)
	if [ "$YML_EXISTS" != 0 ]
	then
		PIPELINE_NAME="$(head -n1 "$PIPELINE"/README.md | sed 's/# //g;')"
		TOOLS="$(sed -n '/### Tools run in the pipeline/ {n;!p;n;p};' "$PIPELINE"/README.md | tr '\n' ' ')"
		SUMMARY="$(sed '/^'"# $PIPELINE_NAME"'/,/^### Tools/!d;//d;' "$PIPELINE/README.md" | tr '\n' ' ' | cut -b 1-300)"

		if [ -f "$PIPELINE/setup.json" ]; then QUICK_SETUP="yes"; fi

		if [ "$SUMMARY" ]; then
			echo "added pipeline: $PIPELINE_NAME"
			echo "| [$PIPELINE_NAME](https://gitlab.com/CinCan/pipelines/-/tree/master/$PIPELINE) | $TOOLS | $SUMMARY | $QUICK_SETUP |" >> README.md
		else
			MSG="$(echo $PIPELINE_NAME |cut -b 1-50)..."
			echo "Pipeline name: $MSG Failure"
		fi
	fi
done

