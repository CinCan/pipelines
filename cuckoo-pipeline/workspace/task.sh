#!/bin/sh
samples="resource-repo/samples/*"
output="resource-cuckoo-analysis/results"

set -e

git clone resource-repo resource-cuckoo-analysis

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"


if [ ! -d "$output" ]; then
    mkdir $output
fi

for filename in $samples
do
    directory=$(basename ${filename%.*})
    job_id=$(curl -F file=@$filename http://172.17.0.1:8090/tasks/create/file | grep -oE "[0-9]+")
    echo $job_id
    echo "Sleep until job done"
    until curl -f http://172.17.0.1:8090/tasks/report/$job_id -so null
    do
	    sleep 5
    done
    echo "Job done!"
    echo "Fetching data..."
    curl -f http://172.17.0.1:8090/tasks/report/$job_id >> $output/$directory-report.json
    curl -f http://172.17.0.1:8090/pcap/get/$job_id >> $output/$directory-dump.pcap
    echo "Done!"
done

cd resource-cuckoo-analysis
git add results/*
git commit -m "dumped data"

