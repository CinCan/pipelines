1. Setup document-pipeline using setup-pipeline.sh script.
2. Modify cincan_zmq.py variable repository with the absolute path to the document-pipeline repository folder on the computer running the script.
3. Modify credentials.yml with the relevant values. Variable mispaddress should be the address to your misp environment. Variable mispkey should be the MISP api key.
