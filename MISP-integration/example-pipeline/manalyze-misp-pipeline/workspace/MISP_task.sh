#!/bin/sh
samples="resource-misp-fetch/samples/"
set -e
git clone resource-repo resource-misp-fetch

git config --global user.email "cincan@cincan.io"
git config --global user.name "cincan"

sed -i "s/True/False/g" /workfolder/keys.py
sed -i "s/<your MISP URL>/$mispaddress/g" /workfolder/keys.py
sed -i "s/Your MISP auth key/$mispkey/g" /workfolder/keys.py

if [ ! -d "$samples" ]; then
    mkdir $samples
fi

echo "starting get_attachment.py"
python /workfolder/get_attachment.py -a $attrID 
mv foo $samples/sample_$attrID
echo "Done"

echo "Updating git"
cd resource-misp-fetch
git add .
git commit -am "Fetched sample"
git push
