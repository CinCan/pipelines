Test pipeline for fetching an attachment from MISP, analyzing it with Manalyze and uploading the results to MISP as an attribute.
Usage: 
1. Create a git repository with the contents of workspace folder.
2. Modify credentials.yml with the relevant values.
3. Setup the pipeline with:
fly -t cincan -p misp-pipeline -c pipeline.yml -l credentials.yml -v attrID:<some_attribute_ID> -v eventID:<some_event_ID>
some_attribute_ID should be the attribute ID of the attachment you wish to analyse. some_event_ID should be the event ID of the attachment.
4. Trigger the pipeline and once it has finished the analysis should show up in both the git repo and MISP. 