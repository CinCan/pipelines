#!/bin/sh
# Run Strings to all but PDF files
SAMPLESPATH=$(pwd)
ls "$SAMPLESPATH"/sample-source/ -R
mkdir -p "$SAMPLESPATH"/output-files/tool-version-info
mkdir -p "$SAMPLESPATH"/output-files/json-output/strings

# Do nothing if folder is empty, otherwise loop through samples.
number_of_files=$(find "$SAMPLESPATH"/sample-source/doc | wc -l)
if [ "$number_of_files" = 0 ]; then
	echo "Folder is empty"
	#date >"$SAMPLESPATH"/output-files/strings-empty.log
else
	# Scan the files
	echo "Processing files"
	apk version busybox | sed -n '2p' | cut -d " " -f 1 >"$SAMPLESPATH"/output-files/tool-version-info/strings
	for file in "$SAMPLESPATH"/sample-source/doc/*; do
		if [ ! -d "$file" ]; then
			echo "Processing file $file"
			hash=$(sha256sum "${file}" | cut -d ' ' -f1)
			# echo "-------------------------------------" | tee -a "$SAMPLESPATH"/output-files/strings.log
			# echo File: "${file##*/}" "$hash" | tee -a "$SAMPLESPATH"/output-files/strings.log
			strings "$file" > tmpfile.txt
			# echo "{\"SHA256\": \"$hash\", \"filename\": \"$file\", \"strings\": \"$file_strings\"}" | jq --null-input --compact-output .
			jq -n --arg hash "$hash" \
			      --arg file "$(basename "$file")" \
				  --rawfile file_strings tmpfile.txt \
				  '{"fileSHA256": $hash, "fileName": $file, "strings": $file_strings}' \
				  > "$SAMPLESPATH/output-files/json-output/strings/strings_$hash.json"
		fi
	done
	date >"$SAMPLESPATH"/output-files/_timestamp_
fi
