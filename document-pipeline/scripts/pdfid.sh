#!/bin/bash
SAMPLESPATH=$(pwd)
ls "$SAMPLESPATH"/sample-source/ -R
mkdir -p "$SAMPLESPATH"/output-files/tool-version-info
mkdir -p "$SAMPLESPATH"/output-files/json-output/pdfid
# Do nothing if folder is empty, otherwise loop through samples.
number_of_files=$(ls -p sample-source/pdf/ | grep -Evc '/')
if [[ "$number_of_files" == 0 ]]; then
	echo "Folder is empty"
	date | tee -a "$SAMPLESPATH"/output-files/pdfid-empty.log
else
	# Scan the files
	echo "-------------------------------------------------"
	echo "Processing " "$number_of_files" "files"...
	cd /pdfid || exit

	ls "$SAMPLESPATH"/sample-source/pdf/
	# Get version information first
	/usr/bin/python pdfid.py --version > "$SAMPLESPATH"/output-files/tool-version-info/pdfid
	# Analyze all sample files. Excluding doc folder
	find "$SAMPLESPATH"/sample-source/pdf/ -type f -not -name ".gitkeep" -not -path "$SAMPLESPATH/sample-source/.git*" -not -path "$SAMPLESPATH/sample-source/doc/*" -exec \
	/usr/bin/python pdfid.py {} -p plugin_triage -j "$SAMPLESPATH"/output-files/json-output/pdfid +

	date > "$SAMPLESPATH"/output-files/_timestamp_
fi

ls -R "$SAMPLESPATH"/output-files/
