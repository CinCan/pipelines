#!/bin/sh

SAMPLESPATH=$(pwd)

# Clone existing results into output-files
git clone results output-files

# Make report of type document-pipeline
pipeline="document-pipeline"

date >"$SAMPLESPATH"/output-files/_timestamp_

cd "$SAMPLESPATH"/output-files/ || exit

standardizer markdown $pipeline "$SAMPLESPATH"/output-files/ -o "$SAMPLESPATH"/output-files/README.md

echo _timestamp_ merge=ours > .gitattributes

ls -la
git checkout results
git config merge.ours.driver true
git config user.email "cincan@cincan.io"
git config user.name "Cincan"
git add .

git commit -m "Final report created"
git pull --rebase
