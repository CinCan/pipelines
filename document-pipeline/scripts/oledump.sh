#!/bin/sh
SAMPLESPATH=$(pwd)
ls "$SAMPLESPATH"/sample-source/ -R
mkdir -p "$SAMPLESPATH"/output-files/tool-version-info
mkdir -p "$SAMPLESPATH"/output-files/json-output/oledump
number_of_files=$(find "$SAMPLESPATH"/sample-source/doc | wc -l)

mkdir -p /tmp/
# Loop through samples, or if folder is empty, create "empty" log.
if [ "$number_of_files" = 0 ]; then
    echo "Folder is empty"
 #   date >"$SAMPLESPATH"/output-files/oledump-empty.log
else
    echo "Processing files"
    /usr/bin/python /oledump/oledump.py --version > "$SAMPLESPATH"/output-files/tool-version-info/oledump

    # Scan the files
    for file in "$SAMPLESPATH"/sample-source/doc/*; do
        if [ ! -d "$file" ]; then
            hash=$(sha256sum "${file}" | cut -d ' ' -f1)
            name=$(basename "$file")
            echo "Analysing file $file"
            # Possible streams are stored into JSON as Base64 encoded binary
            json_filename="$SAMPLESPATH"/output-files/json-output/oledump/oledump_"$hash".json 
            /usr/bin/python /oledump/oledump.py -j -a "$file" > /tmp/oledump_"$hash".json 

            cat /tmp/oledump_"$hash".json
            # Add metada for JSON (filename, filehash sha256)
            jq --arg hash "$hash" \
               --arg name "$name" \
               '.+ {fileSHA256: $hash, fileName: $name}' \
               /tmp/oledump_"$hash".json > "$json_filename"

            echo "Analysis done. Metadata added for file $file."
        fi
    done

    cp "$SAMPLESPATH"/sample-source/doc/*.log "$SAMPLESPATH"/output-files/ 2>/dev/null
    date >"$SAMPLESPATH"/output-files/_timestamp_
fi
