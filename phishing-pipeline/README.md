# Phishing pipeline

Capture screenshots and traffic from URLs mailed to the pipeline

### Tools run in the pipeline



## Setup

`credentials.yml` contains configuration values for the git repository and mail

1. Ingest mail to git
2. Parse URLs from mail body
3. Run headless Chromium and generate
    - PNG image
    - HAR (HTTP archive)
4. Send the files as attachments back to reply-to address

Intermediate results are saved to a git repository

