#!/bin/sh
# Decompile files in the temporary bin-python folder

# Only run if bin-python folder is not empty
if [ "$(ls -A temp-sort-files/bin-python)" ]; then

	# Create output folder
	if [ ! -d "output-files/python-extract-code" ]; then mkdir output-files/python-extract-code; fi

	# Analyze filesin bin-python
	for file in temp-sort-files/bin-python/*; do
		if [ ! -d "$file" ]; then
			echo Analysing: "${file##*/}"
			hash=$(sha256sum "${file}" | cut -d ' ' -f1)
			python3 /python-exe-analysis/extract_code.py "$file"
			mv "${file##*/}"/ output-files/python-extract-code/python-extract-code_"$hash"
		fi
	done

	ls output-files/python-extract-code
fi

