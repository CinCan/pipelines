#!/bin/bash
ls -la

# Get fuzzy hash from source files
if [ ! -z "$(find sample-source -maxdepth 0 -empty)" ]; then
    	echo "Folder is empty"
else
        mkdir -p output-files/ssdc
        echo "Analysing"
	    ssdc sample-source/ -o output-files/ssdc/ssdc-output.tar
fi

cd output-files/ssdc
tar -xvf ssdc-output.tar

ls -la
cat groups.json

