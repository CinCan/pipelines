#!/bin/sh

# Create output folder
if [ ! -d "output-files/peframe" ]; then mkdir output-files/peframe; fi

# Analyze files
for file in sample-source/*; do
	if [ ! -d "$file" ]; then
		echo Analysing: "${file##*/}"
		hash=$(sha256sum "${file}" | cut -d ' ' -f1)

		peframe "$file" -j | tee output-files/peframe/peframe_"$hash".json
	fi
done

ls output-files/peframe
