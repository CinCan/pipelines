#!/bin/sh
# Decompile files in the temporary bin-other folder

# Only run if bin-other folder is not empty
if [ "$(ls -A temp-sort-files/bin-other)" ]; then

        # Create output folder
	if [ ! -d "output-files/snowman-decompile" ]; then mkdir output-files/snowman-decompile; fi

        # Analyze files in bin-other
	for file in temp-sort-files/bin-other/*; do
		if [ ! -d "$file" ]; then
			echo Analysing: "${file##*/}"
			hash=$(sha256sum "${file}" | cut -d ' ' -f1)
			/snowman-master/build/nocode/nocode "$file"  > output-files/snowman-decompile/snowman-decompile_"$hash".json
		fi
	done
	ls output-files/snowman-decompile
fi
