#!/bin/sh
SAMPLESPATH=$(pwd)
OUTPUT_FILES=$SAMPLESPATH/output-files/clamscan

if [ ! -d "$OUTPUT_FILES" ]; then mkdir "$OUTPUT_FILES"; fi

number_of_files=$(find "$SAMPLESPATH"/sample-source | wc -l)

# Do nothing if folder is empty, otherwise loop through samples.
if [ "$number_of_files" = 0 ]; then
    echo "Folder is empty"
    date > "$SAMPLESPATH"/output-files/clamscan-empty.log
else
    freshclam

    add_metadata() {
        file="$1"
        json_file="$2"
        hash=$(sha256sum "${file}" | cut -d ' ' -f1)
        name=$(basename "${file}")
        jq --arg hash "$hash" \
           --arg name "$name" \
           '.+ {fileSHA256: $hash, fileName: $name}' "$json_file" \
           >"$OUTPUT_FILES"/clamav_"$hash".json
    }

    mkdir -p /tmp/clamav/json/
    /usr/bin/clamscan -zr "$SAMPLESPATH"/sample-source/* --exclude="$SAMPLESPATH"/sample-source/.git/ --gen-json --leave-temps --tempdir=/tmp/clamav/ --no-summary

    file /tmp/clamav/*.tmp | grep JSON | cut -d ':' -f1 | xargs -I '{}' mv {} /tmp/clamav/json/

    find "$SAMPLESPATH"/sample-source/* -type f -not -path "$SAMPLESPATH/sample-source/.git*/" | while IFS= read -r sample; do

    # Read clamav generated md5, and based on that identify filename and generate sha256 hash
    md5hash=$(md5sum "${sample}" | cut -d ' ' -f1)
    for f in /tmp/clamav/json/*.tmp; do
	    clamav_md5=$(jq -r ".FileMD5" "$f")
            if [ "$clamav_md5" = "$md5hash" ]; then
                add_metadata "$sample" "$f"
            fi
        done
    done
fi
