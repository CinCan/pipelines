#!/bin/sh

for file in sample-source/*; do
    if [ ! -d "$file" ]; then
        echo Analysing: "${file##*/}"
        hash=$(sha256sum "${file}" | cut -d ' ' -f1)

        mkdir -p output-files/binwalk/binwalk_"$hash"

	# Create txt output, convert to JSON on the next task: standardize-output.sh
        binwalk "$file" -v --dd=".*" -C output-files/binwalk/binwalk_"$hash"/ |tee -a output-files/binwalk/binwalk_"$hash".txt

        echo "Adding file extensions to folder: output-files/binwalk/binwalk_$hash/_${file##*/}.extracted/*"
        ls  output-files/binwalk/binwalk_"$hash"/_"${file##*/}".extracted/* | python3 ./results/add-ext.py --rename

	# Move extracted files to parent directory for simplicity
	mv output-files/binwalk/binwalk_"$hash"/_"${file##*/}".extracted/* output-files/binwalk/binwalk_"$hash"/
	rm output-files/binwalk/binwalk_"$hash"/_"${file##*/}".extracted
    fi
done
ls -la output-files/binwalk/

