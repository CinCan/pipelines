#!/bin/sh
OUTPUT_FILES="$(pwd)"

ls results/

cd /wp1/feature_extractor/ || exit

# Copy configuration files from the "configuration" branch
cp "$OUTPUT_FILES"/configuration/{API.json,active_analyzers} .

for folder in $OUTPUT_FILES/results/ioc_strings/*
    do
        if [ -d "$folder" ]; then
	        mkdir -p "$OUTPUT_FILES"/output-files/feature_extractor/"${folder##*/}"
	        for file in $folder/*
	        do
	            if [ ! -d "$file" ]; then
	                echo Analysing: "${file##*/}"
	                #hash=$(sha256sum "${file}" | cut -d ' ' -f1)
	                python3 analyze_parallel.py --injsonl "$file"
	                filename="${file##*/}"
	                echo CREATED FILE: "$OUTPUT_FILES"/output-files/feature_extractor/"${folder##*/}"/feature_extractor_"${filename/ioc_strings_/}"
	                mv output.json "$OUTPUT_FILES"/output-files/feature_extractor/"${folder##*/}"/feature_extractor_"${filename/ioc_strings_/}"
	            fi
	        done
        fi
    done
ls -R "$OUTPUT_FILES"/output-files/feature_extractor/
