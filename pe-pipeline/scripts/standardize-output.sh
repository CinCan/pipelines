#!/bin/sh
OUTPUT_FILES=$(pwd)
TOOL=$1

# Convert .txt files to .json
for file in "$OUTPUT_FILES"/output-files/"$TOOL"/*; do
	if [ ! -d "$file" ]; then
		if [ "${file##*.}" = "txt" ]; then
			echo "Standardizing output:"
			echo standardizer json -i "$file" -o "${file%.*}".json -t "$TOOL"
	                yes |standardizer json -i "$file" -o "${file%.*}".json -t "$TOOL"
		fi
	fi
done

rm "$OUTPUT_FILES"/output-files/"$TOOL"/*.txt

