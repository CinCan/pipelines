## Concourse pipelines for the pilot environment

These pipelines can be deployed in the [Concourse CI pilot environment](https://gitlab.com/CinCan/environment).

### Descriptions
| Pipeline name  | Tools used  | Description  | Quick setup for the pilot environment |
|----------------|-------------|--------------|---------------------------------------|
| [Email-pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/email-pipeline) | Honeynet Thug / Pywhois  | # Email-pipeline     |  |
| [PDF pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/pdf-pipeline) | PDFiD / PeePDF / JSunpack-n / shellcode analysis  | # PDF pipeline  The pipeline polls for new files at a Gitlab repo, analyses the documents and writes the results to another branch of the repo. * [Watch the VIDEO](https://gitlab.com/CinCan/vault/blob/master/screencast/pdf-pipeline.mp4)   | yes |
| [Virustotal Pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/virustotal-pipeline) | Suricata / iocextract / Virustotal  | # Virustotal Pipeline  This pipeline consumes pcap files from s3 resource compatibible storage, archives pcaps and analyzes the files with suricata and virustotal.     Note: A virustotal api key is required, the free public api key has 4 request/min limit, which will slow down the pipeline.   |  |
| [Cuckoo-pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/cuckoo-pipeline) | Cuckoo sandbox  | # Cuckoo-pipeline  Cuckoo will provide a detailed report outlining the behavior of filea uploaded to Gitlab repository   |  |
| [Concourse Volatility Pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/volatility-pipeline-1) | Volatility  | # Concourse Volatility Pipeline  Concourse pipeline that finds hidden processes and exports their executables to a git repo with Volatility.   |  |
| [Cortex-Abuse_Finder Pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/cortex_pipeline) | Cortex Abuse_Finder  | # Cortex-Abuse_Finder Pipeline  Concourse pipeline that uses Cortex tool Abuse_Finder to analyze data such as IP, email or url.    |  |
| [Phishing pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/phishing-pipeline) |   | # Phishing pipeline  Capture screenshots and traffic from URLs mailed to the pipeline   |  |
| [MISP-integration](https://gitlab.com/CinCan/pipelines/-/tree/master/MISP-integration) |   | # MISP-integration  Example script that uses MISP zmq to listen for events with relevant attachments that could be further analysed with some CinCan pipeline.   |  |
| [Thug pipeline  ](https://gitlab.com/CinCan/pipelines/-/tree/master/thug-pipeline) | Honeynet Thug  | # Thug pipeline    Run a honeyclient (thug) on each URL in a file, get the analysis files in a separate commit   |  |
| [PE-pipeline  ](https://gitlab.com/CinCan/pipelines/-/tree/master/pe-pipeline) | Binwalk/PEframe/ClamAV/snowman-decompiler/python-extract-code/iocstrings/feature_extractor/ghidra/ssdeep/ssdc/osslsigncode  | # PE-pipeline    This pipeline analyses binary files.     | yes |
| [Fuzzy-comparison pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/fuzzycomparison-pipeline) |   | # Fuzzy-comparison pipeline    |  |
| [Document pipeline](https://gitlab.com/CinCan/pipelines/-/tree/master/document-pipeline) | ClamAV/PDFiD/PeePDF/JSunpack-n/shellcode/strings/oledump/olevba  | # Document pipeline  The pipeline clones samples from a Gitlab repo, sorts files to PDF and other documents and then runs appropriate tools to the sample files. * [Watch the VIDEO](https://gitlab.com/CinCan/vault/blob/master/screencast/document-pipeline.mp4)   | yes |
