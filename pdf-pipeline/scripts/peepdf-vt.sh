#!/bin/bash
SAMPLESPATH=$(pwd)
ls $SAMPLESPATH/sample-source/ -R

# Do nothing if folder is empty, otherwise loop through samples.
if (( "$(ls $SAMPLESPATH/sample-source/ |wc -l)" == 0 )); then
	echo "Folder is empty"
        echo $(date) > $SAMPLESPATH/output-files/peepdf-empty.log
else
	echo "Processing files"
	cd /peepdf
	for file in $SAMPLESPATH/sample-source/*
        do
        if [ ! -d $file ]; then
            echo "-------------------------------------------------------"
            echo Analysing: ${file##*/}
            hash=$(sha256sum "${file}" | cut -d ' ' -f1)
    		/usr/bin/python peepdf.py $file -f -c -g > result

    		if (cat result | tee -a $SAMPLESPATH/peepdf.log | grep -c "File not found on VirusTotal");then
			    echo "File not found on VirusTotal"

			    if (cat result | grep -c "Suspicious");then
	    		        echo $hash --- ${file##*/} >> $SAMPLESPATH/peepdf-suspicious.log
				echo "Contains suspicious elements"
			    else
		        	echo $hash --- ${file##*/} >> $SAMPLESPATH/peepdf-clean.log
				    echo "Clean"
			    fi
		    else
		        # File found on Virustotal, and considered malicious by at least some of the engines
			if [[ "$(cat result | tee -a $SAMPLESPATH/peepdf.log | grep Detection: |awk -F'[ |/]' '{print $2}')" == 0 ]]; then
				# File found on Virustotal, but not considered malicious by any engine"
				echo "File found on Virustotal, not considered malicious by any engine"
			else
			        echo "File found on VirusTotal, detected malicious by some engines"
			        cat result | tee -a $SAMPLESPATH/peepdf.log | grep Detection: |awk -F'[ |/]' '{print $2}'
			    	echo $hash --- ${file##*/} >> $SAMPLESPATH/peepdf-virustotal.log
			fi
		    fi
		fi
	done

    # Copy logs to output
    if (( "$(ls $SAMPLESPATH/*.log |wc -l)" != 0 )); then echo "Copy logs" && cp $SAMPLESPATH/*.log $SAMPLESPATH/output-files/; fi
fi
